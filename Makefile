
#
# Exemple de fichier Makefile pour lancer le formattage
# du fichier presentation.tex
#

CIBLES = presentation.pdf

PDFLATEX = pdflatex

all:	$(CIBLES)

presentation.pdf: presentation.tex *.sty img/*
	$(PDFLATEX) presentation.tex
	$(PDFLATEX) presentation.tex

clean:
	rm -f $(CIBLES) *.aux *.log *.blg *.bbl *.out *.toc *.nav *.snm
